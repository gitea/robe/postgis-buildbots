Setup Jenkins on Debian
On 64-bit Debian 6
su root
apt update
apt upgrade #install latest patches
apt install gnupg2 wget
wget -q -O - http://pkg.jenkins-ci.org/debian/jenkins-ci.org.key | apt-key add -

Add to: /etc/apt/sources.list
vi /etc/apt/sources.list
(type o) paste below
deb http://pkg.jenkins-ci.org/debian binary/
escape
:w


## latest version of jenkins requires JDK 7 and one that came with debian is JDK6
## java -version (if not JDK 7 or higher)
sudo su -
echo "deb http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" | tee -a /etc/apt/sources.list
echo "deb-src http://ppa.launchpad.net/webupd8team/java/ubuntu precise main" | tee -a /etc/apt/sources.list

#to get newer proj 4.9.1 needed to do this
echo "deb http://ftp.us.debian.org/debian sid main" | tee -a /etc/apt/sources.list
apt update
apt install libproj9 libproj-dev proj-bin proj-data 
apt install g++ 

#for address_standardizer
apt install libpcre3-dev


apt update
apt install jenkins

##installing java if the above fails
apt-key adv --keyserver keyserver.ubuntu.com --recv-keys EEA14886
apt update
apt install oracle-java7-installer
## end installing java

#for building sfcgal
echo "deb-src http://debian.mirrors.ovh.net/debian/ sid main contrib non-free" | tee -a /etc/apt/sources.list
sudo apt update
apt build-dep sfcgal-bin
apt -b source sfcgal-bin
apt install libcgal-dev libboost-all-dev libmpfr-dev libgmp-dev libopenscenegraph-dev libopenthreads-dev libpq-dev libqt4-dev libqt4-opengl-dev pkg-kde-tools

#for building gdal with odbc support
apt install unixODBC
apt install unixodbc-dev


#make it the default
sudo apt install oracle-java7-set-default
apt install git
apt install git2cl  #for building geos distributions




--to get autoconf etc --
apt install autoconf libtool build-essential
#CUnit
apt install libreadline6 libreadline6-dev zlib1g-dev
apt install libcunit1-dev libcunit1-doc libcunit1
apt install docbook
apt install libxml2-dev n xsltproc docbook-xsl docbook-mathml gettext 
apt-cache search json #see latest version
apt install libjson-c-dev
apt install dblatex
apt install imagemagick
apt install libxml2 libxml2-dev
apt install bison 
apt install libiconv-hook1 
apt install subversion
#(for future translation work)
apt install poxml 
apt install curl
apt install python-dev
#wget https://bitbucket.org/pypa/setuptools/raw/bootstrap/ez_setup.py -O - | sudo python
apt install python-pip

pip install transifex-client

#for doxygen build and  graph generation
apt install doxygen graphviz 
apt install apache2  #(for displaying docbook etc.)
apt install flex (needed for 1.3)
apt install dbtoepub (publish documentation in epub format)
#for building sfcgal, pointcloud
apt install cmake

#for building sfcgal
apt install libboost-dev
apt install libcgal-dev
apt install libsfcgal-dev

#for code coverage
sudo apt install lcov

#for logbt testing
curl -sSfL https://github.com/mapbox/logbt/archive/v2.0.3.tar.gz | tar --gunzip --extract --strip-components=1 --exclude="*md" --exclude="test*" --directory=/usr/local
which logbt
/usr/local/bin/logbt
logbt --version



apt install latex-cjk-xcjk #for Korean and Chinese doc PDF translations

--if you need to update jenkins
aptitude update
apt upgrade
apt install jenkins

apt --only-upgrade install docker-io


-- These are the Jenkins plugins installed --
-- Ones that come preinstalled --
External Monitor Job Type Plugin
LDAP Plugin
pam-auth	
java_doc
Maven Integration Plugin
ant
Jenkins SSH Slaves plugin
Subversion Plugin
CVS Plugin
Translation Assistance Plugin
Managed Script Plugin

-- These are the additional ones I installed --
Blame Upstream Committers
conditional-buildstep
Config File Provider Plugin
GIT client plugin
GIT plugin
Hudson SCP publisher plugin (needed so can create a job that uploads to PostGIS website.  It makes the process the same on windows as on Linux)
Simple Theme Plugin (for changing background and style sheet)
Parameterized Trigger Plugin (for triggering subprojects and passing along main parameters)
Build with Parameters
Doxygen PlugIn
IRC Plugin and Instant Messenger Plugin (so can broadcast to PostGIS IRC)
Managed Scripts
SCM API Plugin
SVN Publisher plugin
Text Finder Run Condition Plugin
Embeddable Build Status Plugin  (for showing image of build status)
Build Authorization Token Root Plugin (for triggering builds via scm hooks)
Generic Webhook plugin (for triggering builds based on gitea web-hook, evenually will use for pull requests)



#needed for reverse proxy https via apache2
#https://wiki.jenkins.io/display/JENKINS/Running+Jenkins+behind+Apache
a2enmod proxy
a2enmod proxy_http
a2enmod headers