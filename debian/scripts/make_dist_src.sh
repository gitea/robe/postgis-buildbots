#!/bin/sh

## begin variables passed in by jenkins

# export PG_VER=9.2
# export PGPORT=8442
# export OS_BUILD=64
# export POSTGIS_MAJOR_VERSION=2
# export POSTGIS_MINOR_VERSION=2
# export POSTGIS_MICRO_VERSION=0dev
# export JENKINS_HOME=/var/lib/jenkins/workspace
# export GEOS_VER=3.4.2dev
# export GDAL_VER=2.0
# export POSTGIS_TAG=trunk
## example release tag ##
# export POSTGIS_TAG=tags/2.1.0
## example branch tag ##
# export POSTGIS_TAG=2.1
# export LIBXML_VER=2.7.8

## end variables passed in by jenkins

PROJECTS=${JENKINS_HOME}/workspace

version=${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION}.${POSTGIS_MICRO_VERSION}

export PGUSER=postgres

export PGPATH=${PROJECTS}/pg/rel/pg${PG_VER}w${OS_BUILD}

export GDAL_DATA="${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/share/gdal"
#if not set then assume trunk
if [ -z "$POSTGIS_TAG" ]; then
	POSTGIS_TAG=trunk
fi

export tag=$POSTGIS_TAG

outdir="${PROJECTS}/postgis/src_dist/postgis-$version"
package="${PROJECTS}/postgis/src_dist/postgis-$version.tar.gz"

rm -rf $outdir
rm $package

if [ -d "$outdir" ]; then
	echo "Output directory $outdir already exists."
	exit 1
fi


echo "Exporting tag ${POSTGIS_TAG}"
svnurl="http://svn.osgeo.org/postgis/${POSTGIS_TAG}"
svn export $svnurl "$outdir"

if [ $? -gt 0 ]; then
exit 1
fi


echo "Removing ignore files, make_dist.sh and HOWTO_RELEASE"
rm -fv "$outdir"/make_dist.sh "$outdir"/HOWTO_RELEASE

# generating configure script and configuring
echo "Running autogen.sh; ./configure"
owd="$PWD"
cd "$outdir"
./autogen.sh
if [ "$POSTGIS_MAJOR_VERSION" -eq "1" ]; then
CPPFLAGS="-I${PGPATH}/pg${PG_VER}/include" \
LDFLAGS="-L${PGPATH}/lib -L${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/lib" ./configure \
  --with-pgconfig=${PGPATH}/bin/pg_config \
  --with-geosconfig=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}/bin/geos-config
else
#PostGIS 2+
CPPFLAGS="-I${PGPATH}/pg${PG_VER}/include" \
LDFLAGS="-L${PGPATH}/lib -L${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/lib" ./configure \
  --with-pgconfig=${PGPATH}/bin/pg_config \
  --with-geosconfig=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}/bin/geos-config \
  --without-raster
fi
# generating postgis_svn_revision.h for >= 2.0.0 tags 
if test -f utils/svn_repo_revision.pl; then 
	echo "Generating postgis_svn_revision.h"
	perl utils/svn_repo_revision.pl $svnurl
fi
#make
cd "$owd"

# generating comments
echo "Generating documentation"
owd="$PWD"
cd "$outdir"/doc
make comments
if [ $? -gt 0 ]; then
	exit 1
fi
make clean # won't drop the comment files
cd "$owd"

# cd "$owd"
# echo "Make java pom file"
# owd="$PWD"
# cd "$outdir"
# cd java/jdbc
# make pom.xml
# if [ $? -gt 0 ]; then
# 	exit 1
# fi

# Run make distclean
echo "Running make distclean"
owd="$PWD"
cd "$outdir"
make distclean


owd="$PWD"
cd "$outdir"


cd ${PROJECTS}/postgis/src_dist
outdir="postgis-$version"
echo "Generating $package file"
tar czf "$package" "$outdir"

cd ${PROJECTS}/postgis/src_dist
POSTGIS_TRUNK_TAG=trunk
# Create php variables for website
# Can't get conditonal logic to work will fix later
if [ "$POSTGIS_TAG" == "$POSTGIS_TRUNK_TAG" ]; 
then
#  ver_file=${POSTGIS_TAG}-version.php
  ver_def=${POSTGIS_TAG}
else
  #ver_file=branch${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION}-version.php
  ver_def=branch${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION}
fi

# echo "<?"> $ver_file
# echo "\$defaults['${ver_def}_version'] = \"${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION}.${POSTGIS_MICRO_VERSION}\"; " >> $ver_file
# echo "\$defaults['${ver_def}_revision'] = \"${POSTGIS_SVN_REVISION}\"; " >> $ver_file
# echo "?>" >> ${ver_file}

POSTGIS_VERSION=${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION}.${POSTGIS_MICRO_VERSION}

#move to website folder
cp $package /var/www/postgis_stuff/
