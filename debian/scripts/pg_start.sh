#!/bin/sh

#
#USAGE set these variables set by Jenkins as params
#WORKSPACE=/var/lib/jenkins/workspace/pg
#PG_VER=9.2
#OS_BUILD=64

export PGPATH=${WORKSPACE}/rel/pg${PG_VER}w${OS_BUILD}
export PGDATA=${PGPATH}/data
export PGDATABASE=postgres
export PGUSER=postgres
export PGLOCALEDIR=${PGPATH}/share/locale
DAEMON=${PGPATH}/bin/postmaster
PGSTARTLOG=${PGDATA}/start_log.log
#${PGPATH}/bin/pg_ctl -D ${PGDATA} -l ${PGDATA}/logfile start  & >> ${PGDATA}/start_logfile 2>&1
$DAEMON -D $PGDATA & >>$PGSTARTLOG 2>&1

