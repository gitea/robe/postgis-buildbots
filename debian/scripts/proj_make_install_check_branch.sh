	#export PROJ_VER=5.2.0
    export PROJ_VER=6.0.0
	#export DATUM_VER=1.8
    #export DATUM_VER=world-1.0RC1
    export DATUM_VER=world-1.0RC1
    export DATUM_VER=north-america-1.1RC1
    export PATH="/mingw/bin:/mingw/include:/mingw/lib:/bin"
    export ZLIB_VER=1.2.11
    export ZLIB_PATH=/projects/zlib/rel-zlib-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}
    #wget  --no-check-certificate http://zlib.net/zlib-1.2.11.tar.gz
    export PATH="/bin:/mingw/bin:/mingw/include:${ZLIB_PATH}/bin:${ZLIB_PATH}/lib:${ZLIB_PATH}/include"
    export PATH=${PROJECTS}/sqlite/rel-sqlite3w${OS_BUILD}${GCC_TYPE}/bin:${PATH}
    export PKG_CONFIG_PATH=${PROJECTS}/sqlite/rel-sqlite3w${OS_BUILD}${GCC_TYPE}/lib/pkgconfig:${PROJECTS}/zlib/rel-zlib-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}/lib
	cd ${SOURCES}
	#rm -rf proj-${PROJ_VER}
	#wget http://download.osgeo.org/proj/proj-${PROJ_VER}.tar.gz
	#wget http://download.osgeo.org/proj/proj-datumgrid-${DATUM_VER}.zip
    #wget https://download.osgeo.org/proj/proj-datumgrid-north-america-1.1RC1.zip
	#tar xvfz proj-${PROJ_VER}.tar.gz
	cd proj-${PROJ_VER}
    make clean
	#cd nad
	#unzip ../../proj-datumgrid-${DATUM_VER}.zip
	#cd ..
	 ./configure --enable-shared --disable-static --prefix=${PROJECTS}/proj/rel-${PROJ_VER}w${OS_BUILD}${GCC_TYPE} --build=${MINGHOST}
    
	#make clean
	make && make install
	cd ${PROJECTS}/proj/rel-${PROJ_VER}w${OS_BUILD}${GCC_TYPE}
	strip bin/*.dll