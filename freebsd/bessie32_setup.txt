Bessie32-12 currently setup on winnie's virtual box - ssh port 51023
Built from: https://download.freebsd.org/ftp/releases/VM-IMAGES/12.0-RELEASE/i386/Latest/FreeBSD-12.0-RELEASE-i386.vmdk.xz(date DEC 2018)

Built from: https://download.freebsd.org/ftp/releases/VM-IMAGES/11.1-RELEASE/i386/Latest/FreeBSD-11.1-RELEASE-i386.vmdk.xz (date July 2017)

(to do admin stuff)
#upgrade to latest patches
freebsd-update fetch
pkg install sudo
Then run:
visudo

and uncomment line allowing wheel to use sudo without password.
Line should look like this when done:
%wheel ALL=(ALL) NOPASSWD: ALL

pkg install bash

vi /etc/rc.conf

add a lines:

hostname="bessie32-12"
sshd_enable="YES"

#then edit the sshd_config
vi /etc/ssh/sshd_config and a line
AllowGroup wheel

Then run:
service sshd start

To add a user user robe
#install keys for each and add to wheel
adduser robe
su robe 
cd ~/
mkdir .ssh
#install keys
#repeat process for each user
adduser komzpa
adduser pramsey
adduser strk
adduser cvvergara
adduser jenkins

repeat for all users (make sure to add admin users to wheel group)

#for adds after user created
pw group mod wheel -m robe
pw group mod sshusers -m jenkins
pw group mod wheel -m jenkins

#Now robe can do 

su root 

or sudo bash
without password.

#pkg install jed tcpdump
pkg install wget git proj geos gdal json-c
pkg install cunit libxml2 bison libxslt #imagemagick only needed for doco
pkg install gmake gdb subversion
pkg install automake autoconf libtool curl bison #needed for configure/lex
pkg install pkgconf #needed for PostGIS 3.0 which now using pkg-config
pkg install protobuf protobuf-c  #needed for building mvt
pkg install sfcgal #for sfcgal will also install cgal

pkg install openjdk11 #for jenkins

#add following lines to /etc/fstab
fdesc   /dev/fd         fdescfs         rw      0       0
proc    /proc           procfs          rw      0       0


pkg install postgresql11-server

#didn't install this yet
#pkg install iconv
sudo bash
edit the /etc/rc.conf 

and add a line
postgresql_enable="YES"

service postgresql initdb
service postgresql start
psql -U postgres
create user jenkins; 
\q

pkg update #update available list
pkg upgrade #upgrades all packages

pkg list | grep proj  #gets list of packages containing proj
pkg info proj  #get details of proj
pkg install cmake #install needed for building pgrouting and sfcgal in future

#for code coverage testing
sudo pkg install lcov

#for pgtap tests needed for pgrouting
git clone https://github.com/theory/pgtap.git
cd pgtap
gmake
sudo gmake install
sudo gmake installcheck PGUSER=postgres
sudo cpan TAP::Parser::SourceHandler::pgTAP


##upgrade to 12.1 (not yet done)
sudo -i
freebsd-update upgrade -r 12.1-RELEASE
freebsd-update install

#reboot
shutdown -r now

#check version running
freebsd-version -k #kernel version
freebsd-version -u #client apps userland - 

#if the -u from above still reads 11.1 need to run update install again
freebsd-update install
#reboot
shutdown -r now