Bessie currently setup on winnie's virtual box - ssh port 51022
Built from https://download.freebsd.org/ftp/releases/VM-IMAGES/12.0-RELEASE/amd64/Latest/FreeBSD-12.0-RELEASE-amd64.vmdk.xz dated (Dec 2018)

vi /etc/rc.conf

add to file

hostname="bessie12"
sshd_enable="YES"

#then edit the sshd_config
vi /etc/ssh/sshd_config 

Add line
AllowGroups wheel
and add to AllowUsers list

vi /etc/ssh/sshd_config #:g/AllowUsers/, :g/PermitRootLogin #change to yes so can configure - can change later to no

#Make sure jenkins is in AllowUsers list of /sshd_config
cat /etc/ssh/sshd_config | grep AllowUsers #if jenkins not listed add



Then run:
service sshd restart

#if get host keys not found run
ssh-keygen -A

To create and add a user robe (repeat for jenkins, pramsey, strk, komzpa, cvvergara) and install relevant ssh keys in homedirs

adduser robe wheel

#for adds after user created
pw group mod wheel -m robe

#Now robe can do 

su root 

(to do admin stuff)
#upgrade to latest patches
freebsd-update fetch

#installing ports
portsnap fetch
portsnap extract

#in future to update ports
portsnap fetch
portsnap update

pkg install sudo

#choose LDAP protocol support in addition to default selected

Then run:

visudo

and uncomment line wheel nopasswd allowing wheel to use sudo without password.

pkg install jed bash tcpdump wget git proj geos
#had these installed, didn't work but worked with openjdk8
pkg install openjdk11

mount -t fdescfs fdesc /dev/fd
mount -t procfs proc /proc


#add following lines to /etc/fstab
fdesc   /dev/fd         fdescfs         rw      0       0
proc    /proc           procfs          rw      0       0

pkg install gmake gdb subversion autoconf libtool curl automake expect
pkg install libxslt libxml2 bison
pkg install imagemagick #if not found try imagemagick7 (was required for FreeBSD 12)
pkg install gdal geos proj
pkg install sfcgal json-c
pkg install cunit iconv
pkg install protobuf protobuf-c  #needed for building mvt

#was going to install postgresql in jail but got lost quickly
#did these steps to enable jails in future
sysctl security.jail.sysvipc_allowed=1

#Make it persistent by adding this line to /etc/sysctl.conf

security.jail.sysvipc_allowed=1

#Add this line to /etc/rc.conf

jail_sysvipc_allow="YES"

#Enter your jail and install Postgres.


pkg install postgresql11-server
service postgresql initdb #create data cluster

edit the /etc/rc.conf 

and add a line
postgresql_enable="YES" 

pkg update #update available list
pkg upgrade #upgrades all packages

pkg info proj  #get details of proj
pkg install cmake #install needed for building pgrouting and sfcgal in future

#for code coverage testing
pkg install lcov

#for pgtap tests needed for pgrouting
git clone https://github.com/theory/pgtap.git
cd pgtap
gmake
sudo gmake install
sudo gmake installcheck PGUSER=postgres
sudo cpan TAP::Parser::SourceHandler::pgTAP


#installing from ports
#installed latest proj from ports by doing https://www.freebsd.org/doc/handbook/ports-using.html
portsnap fetch #only needs to be done once
portsnap extract #only needs to be done once
portsnap fetch update #to keep up to date
cd /usr/ports/graphics/proj
make install
#note you might be asked remove gdal and libgeotiff go ahead
cd /usr/ports/graphics/gdal
make install


##upgrade to 12.1 - not done
sudo -i
freebsd-update fetch
freebsd-update install
freebsd-update upgrade -r 12.1-RELEASE
freebsd-update install

#check version running
freebsd-version -k #kernel version
freebsd-version -u #client apps userland

#reboot
shutdown -r now
