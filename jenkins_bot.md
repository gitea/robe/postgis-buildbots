### Steps to configuring a Jenkins slave bot
Jenkins bots need Java installed
OpenJDK 11 or higher is preferrable

```
adduser jenkins
#for password based authentication answer no
#Make sure jenkins is in AllowUsers list of /sshd_config
cat /etc/ssh/sshd_config | grep AllowUsers #if jenkins not listed add
vi /etc/ssh/sshd_config #:g/AllowUsers/, :g/PermitRootLogin #change to yes so can configure - can change later to no
service sshd restart  

#add key to list
su jenkins
cd ~/
mkdir .ssh

echo ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDJtY+MMy9E+iHf3g8fTlUUnJiZgGVtZC8MAnniU0vMPcK1I6V4PBV60L3vLE/gEph/91wR7VqjGKYZyV9a8Jdl1oQdXJy/s+Y2jjLyYWqLkuFXw4I7NrgBfJxMsSYrk7pCsAgJGEVI6dljU0PnO3oAvJOh7YYJkjA/IMd7bn0nxmbVIyZo2EJLTLJts5sXhfbOqCoM9CdLWUPNp6G4UdBXqjbmTgUvNCu+aR/kDteyaZNuIGjaZcF+6JHPOMQ7izpoyHnNX5mwMoz4shVHi8dzdH+X47v708cV3cfb2F0wqzmFAt4XuwzAmrpPGSfrLX0PViPXwyKPF16waJFdAT4gP7d07F5U4QJq+16BVkph0tpq3VrIDToLPtufxButWvdAi++qdwY2oC/erc1HmAIlBv2o86147H6FiTpq3wrSjwwEVbF+EHsA2OSCbnfKHIwAaEPoaT8w8RxduO5pddU+T5IRTixhZOZ1jUt4mHJkDLmMBWgj7yh0Lyv0C8lF2rCbZACHV3Dq0MtejTMbC6MKDc3X+dzh+KPseZiH6vdMZgMRw4grLkXygK7yCfhlhZ+UvvaUmvuaqZX3VDRIPTMHpKAI3y08mV+orn8JzfVDFMsbc1i9iIllJSf0ZZLf6pjMLc9AxP0zi/PIH+l1RCz9yVnCczuWdY6tMPfOKglTPQ== jenkins_slave@postgis.net >> ~/.ssh/authorized_keys

#if issue with permissions make sure to update
chmod 700 ~/.ssh
chmod 600 ~/.ssh/authorized_keys

```

#### Once you have server ready for jenkins, from jenkins add console
1. Add as a node
2. In the configure it asks for path info for JDK location
3. Specify host name and ssh port
4. Click button to test.  It should ssh in deploy slave.jar etc.

