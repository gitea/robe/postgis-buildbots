#!/bin/bash
# This script will create all data, data1, and data2 database clusters for every installation
# It will also change the default port so it's running on a unique port

maxport=0

for i in `ls`
do 
	if [ -d $i ] ; then
		echo "processing $i"
 
		#rm -rf $i/data0 
                if [ ! -d $i/data0 ] ; then
                        $i/bin/initdb -D $i/data0
                fi;

		# scan for PGPORT currently used so there aren't duplicates
		port=`grep '#*port\s*=' $i/data0/postgresql.conf | sed 's|\(#*port\s*=\s*\)\([0-9]*\)\(.*\)$|\2|g'`
		if [ "$port" -gt "$maxport" ] ; then
			maxport=$port
		fi;
		echo "  data0 -" $port

		
		#rm -rf $i/data1
                if [ ! -d $i/data1 ] ; then
                        $i/bin/initdb -D $i/data1
                fi;
		# scan for PGPORT currently used so there aren't duplicates
                port=`grep '#*port\s*=' $i/data1/postgresql.conf | sed 's|\(#*port\s*=\s*\)\([0-9]*\)\(.*\)$|\2|g'`
                if [ "$port" -gt "$maxport" ] ; then
                        maxport=$port
                fi;
		echo "  data1 -" $port
		
		
		#rm -rf $i/data2
		if [ ! -d $i/data2 ] ; then
			$i/bin/initdb -D $i/data2
		fi;
		# scan for PGPORT currently used so there aren't duplicates
                port=`grep '#*port\s*=' $i/data2/postgresql.conf | sed 's|\(#*port\s*=\s*\)\([0-9]*\)\(.*\)$|\2|g'`
                if [ "$port" -gt "$maxport" ] ; then
                        maxport=$port
                fi;
		echo "  data2 -" $port

		cd $i
		pwd=`pwd`
		cd ..
		# create a startup script for every instance
		sed "s|@@PREFIX@@|$pwd|" pgsql-template | sed "s|@@PGDATA@@|$pwd/data0|" > $i/bin/pgsql0
		sed "s|@@PREFIX@@|$pwd|" pgsql-template | sed "s|@@PGDATA@@|$pwd/data1|" > $i/bin/pgsql1
		sed "s|@@PREFIX@@|$pwd|" pgsql-template | sed "s|@@PGDATA@@|$pwd/data2|" > $i/bin/pgsql2
		chmod +x $i/bin/pgsql0
		chmod +x $i/bin/pgsql1
		chmod +x $i/bin/pgsql2
	fi;
done

# configure a unique port for every database cluster
echo "maxport =" $maxport
for i in `find ./ -name 'postgresql.conf' | sort`
do
	port=`grep '#*port\s*=' $i | sed 's|\(#*port\s*=\s*\)\([0-9]*\)\(.*\)$|\2|g'`
	if [ "$port" -eq 5432 ] ; then
		maxport=`expr $maxport + 1`
		sed -i "s|\(#*port\s*=\s*\)\([0-9]*\)\(.*\)$|port = $maxport\3|" $i
	fi;
done




