#! /bin/sh -xe

# This script performs regression testing of a certain revision of PostGIS against 
# certain versions of PostgreSQL, GEOS, and Proj.
#
# The build enviroment is assumed to have the following layout
# +- ${HOME}/opt/
# |  +- geos/
# |  |  +- 3.2.1
# |  |  +- 3.2.2
# |  +- pgsql
# |  |  +- 8.2.5
# |  |  +- 9.0.1
# |  |  |  +- bin/
# |  |  |  |  +- initdb
# |  |  |  |  +- pgsql0
# |  |  |  |  +- pgsq11
# |  |  |  |  +- pgsql2
# |  |  |  |  +- ...
# |  |  |  +- data0/
# |  |  |  +- data1/
# |  |  |  +- data2/
# |  +- proj/
# |  |  +- 4.6.1
# |  |  +- 4.7.0
#
# GEOS is installed typically by:
#   tar -xvzf geos-3.2.2.tar.bz2
#   cd geos-3.2.2
#   ./configure --prefix=${HOME}/opt/geos/3.2.2
#   make && make install
#
# Proj is installed typically by:
#   tar -xvzf proj-4.7.0.tar.gz
#   cd proj-4.7.0
#   ./configure --prefix=${HOME}/opt/proj/4.7.0
#   make && make install
#
# PostgreSQL is installed typically by:
#   tar -xvzf postgresql-9.0.1.tar.gz
#   cd postgresql-9.0.1
#   ./configure --prefix=${HOME}/opt/pgsql/9.0.1
#   make && make install
#
# The PostgreSQL installations must have three separate instances installed, each
# with their own startup/shutdown scripts (pgsqlX) which point to the appropriate
# database cluster (dataX) installation.  Each instance must run on a unique port across
# all postgres versions.
#
# Essentially, to do a proper regression test, a database instance must be restarted
# to ensure previously loaded libraries are flushed out.  Consequently, only one 
# instance of this script can be invoked at any given time.  Hudson (http://office.refractions.net:1500)
# has a great queuing manager that basically guarentees this.  By having three
# database clusters for every installed postgres version, we can have three
# build queues, each with a call to a specific database cluster environment.  This way,
# we can have one build queue always testing commits to TRUNK.  We can have another
# build queue that can be manually invoked to test arbirary regression tests.  And we can have
# a build queue that performs nightly testing.  In other words, we can perform three separate regression
# tests against the same PostgreSQL version at the same time without clobbering eachother with
# various shutdown/startup commands.
#
# There is a script in http://svn.refractions.net/postgis-autobuild/trunk/scripts/init.sh
# that will create and configure (change default ports) for all three data directories 
# automatically.
#

# input paramters
BRANCH=$1          # either TRUCK, BRANCH_1.5, BRANCH_1.4
REVISION=$2        # revision number of the specified branch to test
PGSQL_VERSION=$3   # the PostgreSQL version number to test against
GEOS_VERSION=$4    # the GEOS version number to test against
PROJ_VERSION=$5    # the PROJ4 version number to test against
GDAL_VERSION=1.9.0    # the GDAL version number to test against
JSON_VERSION=0.9    # the JSON version number to test against
CLUSTER=$6         # the cluster that indicates the database cluster int which to test
                   # "0" should be used in the generic manual tester within Hudson
                   # "1" should be used in the trunk testing script within Hudson
                   # "2" should be used in the branch testing script within Hudson
CUSTOM_SCM=$7      # the directory of an existing SCM checkout (rather than using SVN to retrieve the source code)
                   # this is useful if you want to run many tests over the same source

# record input parameters
echo REVISION=$REVISION > build.properties
echo PGSQL_VERSION=$PGSQL_VERSION >> build.properties
echo GEOS_VERSION=$GEOS_VERSION >> build.properties
echo PROJ_VERSION=$PROJ_VERSION >> build.properties
echo GDAL_VERSION=$GDAL_VERSION >> build.properties
echo JSON_VERSION=$JSON_VERSION >> build.properties

# setup environment
export WORKSPACE=`pwd`

# Add PostgreSQL libraries to path
POSTGRESQL_HOME=${HOME}/opt/pgsql/${PGSQL_VERSION}
export LD_LIBRARY_PATH=${POSTGRESQL_HOME}/lib
export PATH=/usr/local/bin:/bin:/usr/bin:${POSTGRESQL_HOME}/bin
export PGPORT=`grep "#*port\s*=\s*" ${POSTGRESQL_HOME}/data${CLUSTER}/postgresql.conf | sed 's|\(#*port\s*=\s*\)\([0-9]*\)\(.*\)$|\2|g'`

# Add GEOS support
GEOS_HOME=${HOME}/opt/geos/${GEOS_VERSION}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${GEOS_HOME}/lib

# Add Proj support
PROJ_HOME=${HOME}/opt/proj/${PROJ_VERSION}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${PROJ_HOME}/lib

# Add GDAL support
GDAL_HOME=${HOME}/opt/gdal/${GDAL_VERSION}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${GDAL_HOME}/lib

# Add GDAL support
JSON_HOME=${HOME}/opt/json-c/${JSON_VERSION}
export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${JSON_HOME}/lib

# Cleanup old workspace
rm -rf build


# Build project
if [ "$BRANCH" == "TRUNK" ] ; then
	if [ "$CUSTOM_SCM" == "src" ] ; then
		svn export src build
	else
		svn -q -r ${REVISION} export http://svn.osgeo.org/postgis/trunk build
	fi;
	
    cd build
    ./autogen.sh
    ./configure \
       --with-pgconfig=${POSTGRESQL_HOME}/bin/pg_config \
       --with-geosconfig=${GEOS_HOME}/bin/geos-config \
       --with-projdir=${PROJ_HOME} \
       --with-gdalconfig=${GDAL_HOME}/bin/gdal-config \
       --with-jsondir=${JSON_HOME} \
       --with-topology
    make
    #in 2.0/2.1 our temp dir variable changed from TMPDIR to PGIS_REG_TMPDIR
    PGIS_REG_TMPDIR=${WORKSPACE}/results_${PGSQL_VERSION}_${GEOS_VERSION}_${PROJ_VERSION}
elif [ "$BRANCH" == "BRANCH_2.0" ] ; then
        if [ "$CUSTOM_SCM" == "src" ] ; then
                svn export src build
        else
                svn -q -r ${REVISION} export http://svn.osgeo.org/postgis/trunk build
        fi;
     #in 2.0/2.1 our temp dir variable changed from TMPDIR to PGIS_REG_TMPDIR
    PGIS_REG_TMPDIR=${WORKSPACE}/results_${PGSQL_VERSION}_${GEOS_VERSION}_${PROJ_VERSION}
    cd build
    ./autogen.sh
    ./configure \
       --with-pgconfig=${POSTGRESQL_HOME}/bin/pg_config \
       --with-geosconfig=${GEOS_HOME}/bin/geos-config \
       --with-projdir=${PROJ_HOME} \
       --with-gdalconfig=${GDAL_HOME}/bin/gdal-config \
       --with-jsondir=${JSON_HOME} \
       --with-topology
    make
	
elif [ "$BRANCH" == "BRANCH_1.5" ] ; then
	if [ "$CUSTOM_SCM" == "src" ] ; then
		svn export src build
	else
        svn -q -r ${REVISION} export http://svn.osgeo.org/postgis/branches/1.5 build	
	fi;
    
	cd build
    ./autogen.sh
    ./configure \
       --with-pgconfig=${POSTGRESQL_HOME}/bin/pg_config \
       --with-geosconfig=${GEOS_HOME}/bin/geos-config \
       --with-projdir=${PROJ_HOME}
    make

elif [ "$BRANCH" == "BRANCH_1.4" ] ; then
	if [ "$CUSTOM_SCM" == "src" ] ; then
		svn export src build
	else
		svn -q -r ${REVISION} export http://svn.osgeo.org/postgis/branches/1.4 build
	fi;
	
    cd build
    ./autogen.sh
    ./configure \
       --prefix=${WORKSPACE}/prefix/ \
       --with-pgconfig=${POSTGRESQL_HOME}/bin/pg_config \
       --with-geosconfig=${GEOS_HOME}/bin/geos-config \
       --with-projdir=${PROJ_HOME}
    make
    make install
    export LD_LIBRARY_PATH=${LD_LIBRARY_PATH}:${WORKSPACE}/prefix/lib
	
else 
    echo "Invalid specified BRANCH argument:" ${BRANCH}
    exit 1;
fi;


# Restart PostgreSQL so the libraries are properly flushed and reloaded
pgsql${CLUSTER} stop > /dev/null 2>&1 || echo
pgsql${CLUSTER} start


# Verify database has started up
psql -c "SELECT version()" postgres


# Regression Test
if [ `psql -l | grep postgis_reg | wc -l` -gt 0 ] ; then dropdb postgis_reg; fi
#cd regress
export TMPDIR=${WORKSPACE}/results_${PGSQL_VERSION}_${GEOS_VERSION}_${PROJ_VERSION}
mkdir ${TMPDIR}
LOG=${TMPDIR}/regress-${BRANCH}_${REVISION}_${PGSQL_VERSION}_${GEOS_VERSION}_${PROJ_VERSION}.log
echo ------------------------------------- > ${LOG}
echo Regression testing log of PostGIS ${BRANCH} using: >> ${LOG}
cat ${WORKSPACE}/build.properties >> ${LOG}
echo ------------------------------------- >> ${LOG}
echo >> ${LOG}
make check RUNTESTFLAGS='-v' 2>&1 | tee -a ${LOG}
if [ "$?" -eq "0" ]; then
  echo make check returned SUCCESS >> ${LOG}
else
  echo make check returned FAILURE >> ${LOG}
fi
#this doesn't make any sense since these is no regress_log
#mv ${TMPDIR}/regress_log ${TMPDIR}/install-${BRANCH}_${REVISION}_${PGSQL_VERSION}_${GEOS_VERSION}_${PROJ_VERSION}.log

# Cleanup
if [ `psql -l | grep postgis_reg | wc -l` -gt 0 ] ; then dropdb postgis_reg; fi
pgsql${CLUSTER} stop > /dev/null 2>&1 || echo

