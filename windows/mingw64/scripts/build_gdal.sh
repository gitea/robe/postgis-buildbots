SOURCES=/sources
##jenkins passed in variables start here ##
#OS_BUILD=32
#GCC_TYPE=gcc48
#GDAL_VER=1.10.1
#PROJECTS=/projects
##jenkins passed in variables end here ##
GDAL_NAME=gdal-${GDAL_VER}
PREFIX_GDAL=${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}${GCC_TYPE}
PREFIX_LIBICONV=${PROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}${GCC_TYPE}
cd ${PROJECTS}/gdal
#cleandir ${GDAL_NAME}
rm -rf ${GDAL_NAME}
#cleandir ${PREFIX_GDAL}
if [ ! -f ${SOURCES}/${GDAL_NAME}.tar.gz ]; then
  wget -O ${SOURCES}/${GDAL_NAME}.tar.gz http://download.osgeo.org/gdal/${GDAL_VER}/${GDAL_NAME}.tar.gz
fi
tar xvfz ${SOURCES}/${GDAL_NAME}.tar.gz
cd ${GDAL_NAME}

./configure \
--with-curl=no \
--with-threads=no \
--enable-shared \
--with-geos=no \
--with-libz=internal \
--with-libiconv-prefix=${PREFIX_LIBICONV} \
--prefix=${PREFIX_GDAL}
make && make install
strip ${PREFIX_GDAL}/bin/*.dll

