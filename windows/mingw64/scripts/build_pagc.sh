#jenkins provide starts here
#export OS_BUILD=64

#export PATHOLD=$PATH
#export GCC_TYPE=
#export SOURCES=/sources
#export PG_VER=9.2
#export PCRE_VER=8.33

#jenkins provided ends here

export PROJECTS=/projects


if [ "$OS_BUILD" == "64" ] ; then
	export MINGHOST=x86_64-w64-mingw32
else
	export MINGHOST=i686-w64-mingw32
fi;

echo $PATH
export PGHOST=localhost
export PGPORT=5442


export PGUSER=postgres

export PGPATH=${PROJECTS}/postgresql/rel/pg${PG_VER}w${OS_BUILD}${GCC_TYPE}
export PGPATHEDB=${PGPATH}edb
export PATHOLD=".:/bin:/include:/mingw/bin:/mingw/include:/c/Windows/system32:/c/Windows"
export PATH="${PATHOLD}:${PGPATH}/bin:${PGPATH}/lib"
export PATH="${PGPATH}/lib:${PGPATH}/bin:${PROJECTS}/pcre/rel-${PCRE_VER}/bin:${PROJECTS}/pcre/rel-${PCRE_VER}/lib:${PATH}"
if false; then
  cd ${PROJECTS}
  mkdir pcre
  cd pcre
  rm -rf pcre-${PCRE_VER}
  unzip /sources/pcre-${PCRE_VER}.zip
  cd pcre-${PCRE_VER}
  ./configure --build=${MINGHOST} --prefix=${PROJECTS}/pcre/rel-${PCRE_VER}w${OS_BUILD}${GCC_TYPE}
  make && make install
  strip ${PROJECTS}/pcre/rel-${PCRE_VER}w${OS_BUILD}${GCC_TYPE}/bin/*.dll
  rm -rf pcre-${PCRE_VER}
fi

if true; then
  cd ${PROJECTS}/pagc
  #wget -O pagc-postgresql.tgz 'http://pagc.svn.sourceforge.net/viewvc/pagc/branches/sew-refactor/postgresql/?view=tar'
  #svn co svn://svn.code.sf.net/p/pagc/code/branches/sew-refactor/postgresql postgresql
  #rm -rf postgresql
  #tar xzf pagc-postgresql.tgz
  cd postgresql
  
  # createdb test1
  # psql -c 'create extension address_standardizer;' test1
  # psql -f test.sql test1
  # psql -f test2.sql test1
  make clean
  make dist-clean
  export PCRE_PATH=${PROJECTS}/pcre/rel-${PCRE_VER}w${OS_BUILD}${GCC_TYPE}
  make SHLIB_LINK="-L${PCRE_PATH}/lib -lpostgres -lpgport -lpcre" CPPFLAGS="-I.  -I${PCRE_PATH}/include" 
  strip *.dll
  make install
fi;