#!/bin/bash
set -e
#--passed in by Jenkins
#export OS_BUILD=64
#export GCC_TYPE=gcc48
#export PG_VER=9.3
#export LIBXML_VER=2.7.8
#export PGPORT=5443
export PGUSER=postgres


if [[ "${OS_BUILD}" == "" && "$1" == "" ]] ; then
    echo "Usage: makedependencies OS_BUILD"
    echo "       OS_BUILD = 32|64"
    echo "       or export OS_BUILD=xx"
    exit 1
fi

if [[ "${GCC_TYPE}" == "gcc48" ]] ; then
	export PROJECTS=/projects
	export MINGPROJECTS=/projects
	export PATHOLD="/mingw/bin:/mingw/include:/mingw/lib:/c/Windows/system32:/bin:/include:/usr/local/bin"
else
	export PROJECTS=/projects
	export MINGPROJECTS=/projects
	export PATHOLD="/mingw/bin:/mingw/include:/mingw/lib:/c/Windows/system32:/bin:/include:/usr/local/bin"
fi;
#alias cmake="/c/ming${OS_BUILD}/cmake-2.8.10.2-win32-x86/bin/cmake"
#alias ctest="/c/ming${OS_BUILD}/cmake-2.8.10.2-win32-x86/bin/ctest"
CMAKE_PATH=/cmake


cd ${PROJECTS}
export PATH="${PATHOLD}:${CMAKE_PATH}/bin"

export PGPATH=${PROJECTS}/postgresql/rel/pg${PG_VER}w${OS_BUILD}${GCC_TYPE}
export PGPATHEDB=${PROJECTS}/postgresql/rel/pg${PG_VER}w${OS_BUILD}${GCC_TYPE}edb
export PATH="${PGPATH}:${PATH}"
cd ${PROJECTS}/postgis

#mkdir pointcloud
ZLIB_VER=1.2.11


#-- build pointcloud extension --
if true; then
	
	cd ${PROJECTS}/postgis/pointcloud

	#https://docs.google.com/document/d/1qJjiNUleh220so1gwthTtyJuBZj0TD3eDMTql-djCk8/edit?pli=1
	export PATH="${PATH}:${PROJECTS}/zlib/rel-zlib-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}/bin:${PROJECTS}/zlib/rel-zlib-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}/lib:${PROJECTS}/zlib/rel-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/bin:${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/lib:${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/include:${PGPATH}/include:${PGPATH}/bin"
	#rm -rf pointcloud
	#export PATH="${PGPATH}:${PATHOLD}:${PROJECTS}/gtkw${OS_BUILD}${GCC_TYPE}/bin" #${PROJECTS}/gtkw${OS_BUILD}${GCC_TYPE}/bin: 
	#git clone -b master https://github.com/pramsey/pointcloud.git
	cd pointcloud
    # dos2unix configure.ac
    # dos2unix autogen.sh
    # dos2unix config.mk.in
    # dos2unix lib/pc_config.h.in
	if [ -e ./GNUMakefile ]; then
		#make distclean
		make clean
	fi
	sh autogen.sh

	#./configure --help
	#exit;
	export CPPFLAGS="-I${PROJECTS}/zlib/rel-zlib-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}/include  -I${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/include"
	export LDFLAGS="-L${PROJECTS}/zlib/rel-zlib-${ZLIB_VER}w${OS_BUILD}${GCC_TYPE}/lib -L${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/lib"
	./configure --target=${MINGHOST} --with-xml2config=${PROJECTS}/libxml/rel-libxml2-${LIBXML_VER}w${OS_BUILD}${GCC_TYPE}/bin/xml2-config \
		--with-pgconfig=${PGPATH}/bin/pg_config
	make clean && make
	make install
	#make check
	
	cp pgsql/*.sql ${PGPATHEDB}/share/extension
	cp -r pgsql/*.control ${PGPATHEDB}/share/extension
	cp -r pgsql/*.dll ${PGPATHEDB}/lib
	
	cp pgsql_postgis/*.sql ${PGPATHEDB}/share/extension
	cp -r pgsql_postgis/*.control ${PGPATHEDB}/share/extension
	cd pgsql
	#make installcheck
	cd ..
	export GIT_REVISION=`git status | head -1`

	
fi

if [ "$?" != "0" ]; then
	 exit $?
fi

#only package if make check succeeded (note took out make check succeed check cause new tests we dont build dependencies for)
if [ "$?" == "0" ]; then
	strip pgsql/*.dll
	export REL_PGVER=${PG_VER//./} #strip the period
	export RELDIR=${PROJECTS}/postgis/pointcloud/builds
	export RELVERDIR=pointcloud-pg${REL_PGVER}-binaries-${POINTCLOUD_VER}w${OS_BUILD}${GCC_TYPE}
	export PATH="${PATHOLD}:${PGPATH}/bin:${PGPATH}/lib"
	export outdir="${RELDIR}/${RELVERDIR}"
	export package="${RELDIR}/${RELVERDIR}.zip"
	export verfile="${RELDIR}/${RELVERDIR}/pgpointcloud_version.txt"
	rm -rf $outdir
	rm -f $package
	mkdir -p $outdir
	mkdir -p $outdir/share/extension
	mkdir $outdir/lib
	cp pgsql/*.sql ${RELDIR}/${RELVERDIR}/share/extension
	cp -r pgsql/*.control ${RELDIR}/${RELVERDIR}/share/extension
	cp -r pgsql/*.dll ${RELDIR}/${RELVERDIR}/lib
	cp -r COPYRIGHT ${RELDIR}/${RELVERDIR}/pgpointcloud_COPYRIGHT
	
	cp pgsql_postgis/*.sql ${RELDIR}/${RELVERDIR}/share/extension
	cp -r pgsql_postgis/*.control ${RELDIR}/${RELVERDIR}/share/extension
	
	cp -r ${RELDIR}/packaging_notes/* ${RELDIR}/${RELVERDIR}/
	
	echo "POSTGIS POINTCLOUD: ${POINTCLOUD_VER}" > $verfile
	echo "PostgreSQL: ${PG_VER} w${OS_BUILD}" >> $verfile
	date_built="`eval date +%Y%m%d`"
	echo "Built: ${date_built}" >> $verfile
	echo "GIT_REPO: https://github.com/pointcloud/pointcloud" >> $verfile 
	echo "GIT_REVISION: ${GIT_REVISION}" >> $verfile

	cd ${RELDIR}
	zip -r $package ${RELVERDIR}

	cp $package ${PROJECTS}/postgis/win_web/download/windows/pg${REL_PGVER}/buildbot/extras
fi
