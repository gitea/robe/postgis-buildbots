#!/bin/bash
set -e
export PROJECTS=/projects
cd ${PROJECTS}/cgal/sfcgal/branches/${SFCGAL_VER}
## settings from jenkins start
#export SFCGAL_VER=1.2
#CGAL_VER=4.6.1
## seetings from jenkins end

GMP_VER=5.1.2
MPFR_VER=3.1.2

BOOST_VER=1.59.0

BOOST_VER_WU=1_59_0

export PATH="/mingw/bin:/mingw/include:/c/Windows/system32" 
PATH="${PATH}:${PROJECTS}/CGAL/rel-gmp-${GMP_VER}w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/CGAL/rel-gmp-${GMP_VER}w${OS_BUILD}${GCC_TYPE}/lib:${PROJECTS}/CGAL/rel-gmp-${GMP_VER}w${OS_BUILD}${GCC_TYPE}/bin"
PATH="${PATH}:${PROJECTS}/CGAL/rel-mpfr-${MPFR_VER}w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/CGAL/rel-mpfr-${MPFR_VER}w${OS_BUILD}${GCC_TYPE}/lib:${PROJECTS}/CGAL/rel-mpfr-${MPFR_VER}w${OS_BUILD}${GCC_TYPE}/bin"
PATH="${PATH}:${PROJECTS}/CGAL/rel-cgal-${CGAL_VER}w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/CGAL/rel-cgal-${CGAL_VER}w${OS_BUILD}${GCC_TYPE}/lib"
#PATH="${PATH}:${PROJECTS}/CGAL/gmp-${GMP_VER}"
PATH="${PATH}:.:/bin:/include"

#export PATH="/mingw/bin:/mingw/include:/c/Windows/system32" 
PATH="${PATH}:${PROJECTS}/CGAL/rel-gmp-${GMP_VER}w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/CGAL/rel-gmp-${GMP_VER}w${OS_BUILD}${GCC_TYPE}/lib:${PROJECTS}/CGAL/rel-gmp-${GMP_VER}w${OS_BUILD}${GCC_TYPE}/bin"
PATH="${PATH}:${PROJECTS}/CGAL/rel-mpfr-${MPFR_VER}w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/CGAL/rel-mpfr-${MPFR_VER}w${OS_BUILD}${GCC_TYPE}/lib:${PROJECTS}/CGAL/rel-mpfr-${MPFR_VER}w${OS_BUILD}${GCC_TYPE}/bin"
PATH="${PATH}:${PROJECTS}/CGAL/rel-cgal-${CGAL_VER}w${OS_BUILD}${GCC_TYPE}/include:${PROJECTS}/CGAL/rel-cgal-${CGAL_VER}w${OS_BUILD}${GCC_TYPE}/lib"
#PATH="${PATH}:${PROJECTS}/CGAL/gmp-${GMP_VER}"
#PATH="${PATH}:.:/bin:/include"

export PATH="${PATH}:/cmake/bin"
#cd ${PROJECTS}/CGAL


#wget --no-check-certificate -O SFCGAL-${SFCGAL_VER}.tar.gz https://github.com/Oslandia/SFCGAL/archive/v${SFCGAL_VER}.tar.gz 
export MPFR_DIR=${PROJECTS}/CGAL/rel-mpfr-${MPFR_VER}w${OS_BUILD}${GCC_TYPE}
export GMP_DIR=${PROJECTS}/CGAL/rel-gmp-${GMP_VER}w${OS_BUILD}${GCC_TYPE}
export CPPFLAGS="-I${PROJECTS}/CGAL/rel-gmp-${GMP_VER}w${OS_BUILD}${GCC_TYPE}/include"
export LDFLAGS="-L${PROJECTS}/CGAL/rel-gmp-${GMP_VER}w${OS_BUILD}${GCC_TYPE}/lib"

cd ${PROJECTS}/cgal/sfcgal
#make sure to change file src/CMakeList.txt move MPFR check before GMP
rm -rf build${OS_BUILD}${GCC_TYPE}
mkdir -p build${OS_BUILD}${GCC_TYPE}
cd build${OS_BUILD}${GCC_TYPE}
cmake -G "MSYS Makefiles" -DCMAKE_BUILD_TYPE:STRING=Release -DCMAKE_INSTALL_PREFIX:PATH=${PROJECTS}/CGAL/rel-sfcgal-${SFCGAL_VER}w${OS_BUILD}${GCC_TYPE} -DBoost_USE_STATIC_LIBS=ON -DCMAKE_CXX_FLAGS:STRING="-DBOOST_THREAD_USE_LIB -DBoost_USE_STATIC_LIBS -DBOOST_USE_WINDOWS_H" -DBOOST_ROOT:PATH=${PROJECTS}/boost/rel-${BOOST_VER_WU}w${OS_BUILD}${GCC_TYPE} ${PROJECTS}/cgal/sfcgal/branches/${SFCGAL_VER}

make
make install
cd ${PROJECTS}/CGAL/rel-sfcgal-${SFCGAL_VER}w${OS_BUILD}${GCC_TYPE}/bin
strip *.dll
#config file looks for dll in the lib folder instead of bin
cp ${PROJECTS}/CGAL/rel-sfcgal-${SFCGAL_VER}w${OS_BUILD}${GCC_TYPE}/bin/*.dll ${PROJECTS}/CGAL/rel-sfcgal-${SFCGAL_VER}w${OS_BUILD}${GCC_TYPE}/lib

#replace windows path format (drive latter) with unix path format
sed -i "/prefix=./c\prefix=${PROJECTS}/CGAL/rel-sfcgal-${SFCGAL_VER}w${OS_BUILD}${GCC_TYPE}"  ${PROJECTS}/CGAL/rel-sfcgal-${SFCGAL_VER}w${OS_BUILD}${GCC_TYPE}/bin/sfcgal-config
