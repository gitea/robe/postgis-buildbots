## begin variables passed in by jenkins

#export GEOS_VER=3.4.3dev 
#export GEOS_MINOR_VER=3.4
#export PROJECTS=/c/jenkins 
#export SH_WORKSPACE=/c/jenkins/geos 
#export OS_BUILD=64
#export MAKE_TYPE=cmake
#export GCC_TYPE=gcc48

export SH_WORKSPACE=${PROJECTS}/geos
if [[ "$GCC_TYPE" == "gcc45" ]] ; then #gcc 45 has no extension
  export GCC_TYPE=
fi;
## end variables passed in by jenkins

if [ "$OS_BUILD" == "64" ] ; then
	export MINGHOST=x86_64-w64-mingw32
else
	export MINGHOST=i686-w64-mingw32
fi;
CMAKE_PATH=/cmake
cd ${SH_WORKSPACE}/branches/${GEOS_MINOR_VER}${MAKE_TYPE}

if [ -e ./GNUMakefile ]; then
 make distclean
fi

#sh autogen.sh
if [[ "${GEOS_VER}"  == *SVN* || "${GEOS_VER}"  == *dev* ]] ; then
  export SRC_DIR=${PROJECTS}/geos/branches/${GEOS_MINOR_VER}${MAKE_TYPE}
  export CMAKE_GEOS_VER=${GEOS_MINOR_VER}${MAKE_TYPE}
else
  #tagged version -- official release
  export SRC_DIR=${PROJECTS}/geos/tags/${GEOS_VER}${MAKE_TYPE}
  export CMAKE_GEOS_VER=${GEOS_VER}${MAKE_TYPE}
fi;

cd ${SRC_DIR}
#sh autogen.sh 
if [[ "$MAKE_TYPE"  == *cmake*  ]] ; then
    export PATH="${PATH}:${CMAKE_PATH}/bin:/bin:/include"
   echo $SRC_DIR
   export PATH="${PATH}:${CMAKE_PATH}/bin:/bin:/include"
    #if building from svn
   
    #tools/svn_repo_revision.sh 
    cd ../
    rm -rf build${OS_BUILD}${MAKE_TYPE}
    mkdir -p build${OS_BUILD}${MAKE_TYPE}
    cd build${OS_BUILD}${MAKE_TYPE}
    export GEOS_ENABLE_INLINE=ON #for older geos
    if [[ "${DISABLE_GEOS_INLINE}" == "" ]] ; then
      export DISABLE_GEOS_INLINE=ON
    fi 
    echo "Disable geos inline ${DISABLE_GEOS_INLINE}"
  
    if [[ "${OS_BUILD}" == "64" ]] ; then
      #cmake -G "MSYS Makefiles" DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE} -DHAVE_STD_ISNAN=1 -DHAVE_LONG_LONG_INT_64=1 -DGEOS_ENABLE_INLINE=NO -DDISABLE_GEOS_INLINE=ON   -  ../${CMAKE_GEOS_VER}
      cmake -G "MSYS Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE} -DHAVE_STD_ISNAN=1 -DHAVE_LONG_LONG_INT_64=1 -DGEOS_ENABLE_INLINE=${GEOS_ENABLE_INLINE} -DDISABLE_GEOS_INLINE=${DISABLE_GEOS_INLINE}  -  ../${CMAKE_GEOS_VER}
    else
      #cmake -G "MSYS Makefiles" DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE} -DHAVE_STD_ISNAN=1 -DGEOS_ENABLE_INLINE=NO -DDISABLE_GEOS_INLINE=ON   -  ../${CMAKE_GEOS_VER}
      cmake -G "MSYS Makefiles" -DCMAKE_BUILD_TYPE=Release -DCMAKE_INSTALL_PREFIX:PATH=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE} -DHAVE_STD_ISNAN=1 -DGEOS_ENABLE_INLINE=${GEOS_ENABLE_INLINE} -DDISABLE_GEOS_INLINE=${DISABLE_GEOS_INLINE}  -  ../${CMAKE_GEOS_VER}
    fi
  make && make install
  #make check
  ctest --output-on-failure
else
	#assume mingw libtool
	  cd $SRC_DIR
	  sh autogen.sh
	  #cd ../
    #rm -rf build${OS_BUILD}${MAKE_TYPE}
    #mkdir -p build${OS_BUILD}${MAKE_TYPE}
    #cd build${OS_BUILD}${MAKE_TYPE}
	  configure --build=${MINGHOST} --prefix=${SH_WORKSPACE}/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE} 
    make && make install
    make check 
fi;


strip ${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}${GCC_TYPE}/bin/*.dll  


