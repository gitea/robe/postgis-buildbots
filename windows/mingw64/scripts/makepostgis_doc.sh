#!/bin/sh
#### $Id:makepostgis_doc.sh 10208 2012-08-30 05:38:22Z robe $
#remarked out variables passed in by jenkins
#OS_BUILD=32
export PROJECTS=/c/jenkins
#POSTGIS_MAJOR_VERSION=2
#POSTGIS_MINOR_VERSION=1
#POSTGIS_MICRO_VERSION=0SVN
#POSTGIS_SVN_REVISION=test #jenkins passes this in
export POSTGIS_VER=${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION}

export POSTGIS_MICRO_VER=${POSTGIS_MAJOR_VERSION}.${POSTGIS_MINOR_VERSION}.${POSTGIS_MICRO_VERSION}
export POSTGIS_SRC=${PROJECTS}/postgis/branches/${POSTGIS_VER}
export MINGPROJECTS=/c/ming${OS_BUILD}/projects

if [ "$OS_BUILD" == "64" ] ; then
	export MINGHOST=x86_64-w64-mingw32
else
	export MINGHOST=i686-w64-mingw32
fi;

export PATHOLD=$PATH


export PATHOLD=".:/bin:/include:/mingw/bin:/mingw/include:/c/ming${OS_BUILD}/imagemagick:/c/Windows/system32:/c/Windows:/usr/local/bin:/c/ming${OS_BUILD}/svn:${PROJECTS}/xsltproc:${PROJECTS}"
#export PG_VER=9.2rc1
export PGWINVER=${PG_VER}edb
export PATH="${PATHOLD}:${PGPATH}/bin:${PGPATH}/lib"
export PATH="${MINGPROJECTS}/gettext/rel-gettext-0.18.1/bin:${MINGPROJECTS}/xsltproc:${MINGPROJECTS}/gtk/bin:${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}/bin:${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/include:${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/bin:${PATH}"

echo $PATH
export GEOS_VER=3.4.0dev
export GDAL_VER=1.9.1
export PROJSO=libproj-0.dll


#export POSTGIS_SRC=${PROJECTS}/postgis/trunk
export GDAL_DATA="${PROJECTS}/pgx64/pg${PG_VER}/gdal-data"
export PROJ_VER=4.8.0
cd ${PROJECTS}
export PGPATH=${PROJECTS}/postgresql/rel/pg${PG_VER}w${OS_BUILD}
#mkdir postgis

svn update ${POSTGIS_SRC}
cd ${POSTGIS_SRC}


make clean
sh autogen.sh

CPPFLAGS="-I${PGPATH}/include -I${MINGPROJECTS}/gettextrel-gettext-0.18.1/include -I${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/include" \
LDFLAGS="-L${PGPATH}/lib -L${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/lib -L${MINGPROJECTS}/gettext/rel-gettext-0.18.1/lib -L${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD}/lib" ./configure \
 --host=${MINGHOST}  \
  --with-xml2config=${MINGPROJECTS}/libxml/rel-libxml2-2.7.8w${OS_BUILD}/bin/xml2-config \
  --with-pgconfig=${PGPATH}/bin/pg_config \
  --with-geosconfig=${PROJECTS}/geos/rel-${GEOS_VER}w${OS_BUILD}/bin/geos-config \
  --with-projdir=${MINGPROJECTS}/proj/rel-${PROJ_VER}w${OS_BUILD} \
  --with-gdalconfig=${PROJECTS}/gdal/rel-${GDAL_VER}w${OS_BUILD}/bin/gdal-config \
  --with-jsondir=${MINGPROJECTS}/json-c/rel-0.9w${OS_BUILD} \
  --with-libiconv=${MINGPROJECTS}/rel-libiconv-1.13.1w${OS_BUILD} \
  --with-xsldir=${MINGPROJECTS}/docbook/docbook-xsl-1.76.1 \
  --with-gui --with-gettext=no
cd doc
mv postgis.xml postgis.xml.orig
sed -e "s:</title>:</title><subtitle><subscript>SVN Revision (<emphasis>${POSTGIS_SVN_REVISION}</emphasis>)</subscript></subtitle>:" postgis.xml.orig > postgis.xml
make -e chunked-html 2>&1 | tee -a doc-errors.log
make chunked-html
mv postgis.xml.orig postgis.xml
